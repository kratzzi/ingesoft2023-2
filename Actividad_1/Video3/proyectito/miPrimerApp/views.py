from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):

    varEstudiante = Estudiante.objects.filter(grupo=1)
    return render(request, 'index.html',{'varEstudiante':varEstudiante})
